package com.android.bitcointicker.presentation.tab

import android.os.Bundle
import com.android.bitcointicker.R
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentTabBinding
import com.android.bitcointicker.presentation.favorite.FavoriteFragment
import com.android.bitcointicker.presentation.home.HomeFragment
import com.android.bitcointicker.presentation.profile.ProfileFragment
import com.android.easynav.src.FragmentController
import com.android.easynav.src.animation.Animation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class TabFragment : BaseFragment<TabVM, FragmentTabBinding>() {

    override val layoutRes = R.layout.fragment_tab
    override fun getViewModelClass() = TabVM::class.java

    private val homeFragment = HomeFragment.newInstance()
    private val favoriteFragment = FavoriteFragment.newInstance()
    private val profileFragment = ProfileFragment.newInstance()

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)


        navigator.apply {
            createChildContainer(R.id.tab_container)
            setAnimation(Animation.build { })
            createBottomMenu(
                FragmentController.CHILD_CONTROLLER,
                binding.tabBottomNavigation,
                listOf(homeFragment, favoriteFragment, profileFragment)
            )
        }
    }

    companion object {
        fun newInstance() = TabFragment()
    }


}

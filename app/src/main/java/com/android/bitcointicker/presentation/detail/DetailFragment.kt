package com.android.bitcointicker.presentation.detail

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.Observer
import com.android.bitcointicker.R
import com.android.bitcointicker.core.Observe
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentDetailBinding
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.domain.entity.Favorite
import com.android.bitcointicker.utility.constants.BundleConstants
import com.android.bitcointicker.utility.extension.fieldFocusListener
import com.android.bitcointicker.utility.extension.onSubmit
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class DetailFragment : BaseFragment<DetailVM, FragmentDetailBinding>() {
    override val layoutRes = R.layout.fragment_detail
    override fun getViewModelClass() = DetailVM::class.java

    private var coinId = ""
    private var interValSec: Int = 10
    private var coinEntity: CoinEntity? = null

    val handler = Handler(Looper.getMainLooper())

    override fun define() {
        super.define()

        coinId = arguments?.getString(BundleConstants.COIN_ID, "") ?: ""
    }

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)



        handler.postDelayed(object : Runnable {
            override fun run() {
                viewModel.getCoinDetail(coinId)
                handler.postDelayed(this, (interValSec * 1000).toLong())
            }
        }, (interValSec * 1000).toLong())



        binding.activityCoinDetailRefreshIntervalEdittext.setText(interValSec.toString())
    }


    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.coin.observe(viewLifecycleOwner, coin)
        viewModel.favorite.observe(viewLifecycleOwner, fav)
    }

    override fun runOnce() {
        super.runOnce()
        viewModel.getCoinDetail(coinId)
    }

    override fun clickListeners() {
        super.clickListeners()

        binding.fragmentDetailFavButton.setOnClickListener {
            viewModel.changeFavorite(coinId, coinEntity)
        }

        binding.activityCoinDetailRefreshIntervalEdittext.fieldFocusListener {
            binding.fragmentRootNestedscrollview.post {
                binding.fragmentRootNestedscrollview.fullScroll(View.FOCUS_DOWN)
            }
        }

        binding.activityCoinDetailRefreshIntervalEdittext.onSubmit {
            interValSec=Integer.parseInt(binding.activityCoinDetailRefreshIntervalEdittext.text.toString())
        }

    }

    private val coin = Observe<CoinEntity> {
        it.handle(loading = {
            showLoading()
        }, success = { coin ->
            coinEntity = coin
            binding.data = coin

        }, hideLoading = {
            hideLoading()
        })

    }

    private val fav = Observer<Favorite> {
        binding.fav = it
    }

    companion object {
        fun newInstance(coinId: String?): DetailFragment {

            val fragment = DetailFragment()

            val bundle = Bundle()
            bundle.putString(BundleConstants.COIN_ID, coinId)
            fragment.arguments = bundle

            return fragment
        }
    }
}
package com.android.bitcointicker.presentation.adapter


import android.view.ViewGroup
import com.android.bitcointicker.R
import com.android.bitcointicker.core.adapter.BaseAdapter
import com.android.bitcointicker.core.adapter.BaseViewHolder
import com.android.bitcointicker.core.adapter.RecyclerItem
import com.android.bitcointicker.databinding.ReyclerCellCoinBinding
import com.android.bitcointicker.databinding.ReyclerCellSearchBinding
import com.android.bitcointicker.databinding.ReyclerCellTitleBinding
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.utility.enums.AdapterEnum

class CoinAdapter : BaseAdapter<RecyclerItem>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<RecyclerItem, *> {
        return when (viewType) {
            CoinViewHolder.TYPE -> CoinViewHolder(parent)
            TitleViewHolder.TYPE -> TitleViewHolder(parent)
            SearchViewHolder.TYPE -> SearchViewHolder(parent)
            else -> CoinViewHolder(parent)
        } as BaseViewHolder<RecyclerItem, *>

    }


    class CoinViewHolder(parent: ViewGroup) :
        BaseViewHolder<CoinEntity, ReyclerCellCoinBinding>(
            parent,
            R.layout.reycler_cell_coin
        ) {

        override fun bind(item: CoinEntity, binding: ReyclerCellCoinBinding) {
            with(binding) {
                data = item
                executePendingBindings()
            }
        }

        companion object {
            val TYPE = AdapterEnum.CoinEnum.COIN.type
        }

    }

    class TitleViewHolder(parent: ViewGroup) : BaseViewHolder<TitleItem, ReyclerCellTitleBinding>(
        parent,
        R.layout.reycler_cell_title
    ) {
        override fun bind(item: TitleItem, binding: ReyclerCellTitleBinding) {
            with(binding) {
                data = item
                executePendingBindings()
            }
        }

        companion object {
            val TYPE = AdapterEnum.CoinEnum.TITLE.type
        }

    }

    class SearchViewHolder(parent: ViewGroup) :
        BaseViewHolder<CoinEntity, ReyclerCellSearchBinding>(
            parent,
            R.layout.reycler_cell_search
        ) {
        override fun bind(item: CoinEntity, binding: ReyclerCellSearchBinding) {
            with(binding) {
                data = item
                executePendingBindings()
            }
        }

        companion object {
            val TYPE = AdapterEnum.CoinEnum.SEARCH.type
        }

    }


}
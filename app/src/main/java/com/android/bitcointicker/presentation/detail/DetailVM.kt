package com.android.bitcointicker.presentation.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.bitcointicker.core.LiveWithDataState
import com.android.bitcointicker.core.ObserveData
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.domain.entity.Favorite
import com.android.bitcointicker.domain.mapper.toFieldMap
import com.android.bitcointicker.domain.usecase.CoinUseCase
import com.android.bitcointicker.domain.usecase.FavoriteUseCase
import com.android.bitcointicker.utility.LiveData.default
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class DetailVM @ViewModelInject constructor(
    private val coinUseCase: CoinUseCase,
    private val favoriteUseCase: FavoriteUseCase
) :
    BaseViewModel() {

    private val _coin = ObserveData<CoinEntity>()

    val coin: LiveWithDataState<CoinEntity>
        get() = _coin

    val favorite = MutableLiveData<Favorite>().default(Favorite(false))

    fun getCoinDetail(id: String) {

        execute { coinUseCase.getCoinDetail(id) }.onEach {
            _coin.value = it
        }.launchIn(viewModelScope)

        favoriteUseCase.favoriteCheck(id) { bool ->
            favorite.value?.isFavorite = bool
        }
    }

    fun changeFavorite(coinId: String, coinEntity: CoinEntity?) {
        coinEntity?.let {
            favorite.value?.isFavorite?.let { isFav ->
                if (isFav) {
                    favoriteUseCase.removeFavorite(coinId) {

                    }
                    favorite.value?.isFavorite = false
                } else {
                    favoriteUseCase.saveFavorite(it.toFieldMap()) {

                    }
                    favorite.value?.isFavorite = true
                }
            }
        }


    }


}
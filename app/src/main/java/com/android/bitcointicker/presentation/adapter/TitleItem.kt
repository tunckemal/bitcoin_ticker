package com.android.bitcointicker.presentation.adapter

import com.android.bitcointicker.core.adapter.RecyclerItem

data class TitleItem(
    var title: String
) : RecyclerItem(CoinAdapter.TitleViewHolder.TYPE, "")
package com.android.bitcointicker.presentation.profile

import android.os.Bundle
import androidx.lifecycle.Observer
import com.android.bitcointicker.R
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentProfileBinding
import com.android.bitcointicker.presentation.auth.login.LoginFragment
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class ProfileFragment : BaseFragment<ProfileVM, FragmentProfileBinding>() {

    override val layoutRes = R.layout.fragment_profile
    override fun getViewModelClass() = ProfileVM::class.java


    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)

        binding.fragmentProfileNotificationSwitch.isChecked = viewModel.getAllowNotification()
    }

    override fun runOnce() {
        super.runOnce()

        viewModel.getCurrentUser()
    }

    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.user.observe(viewLifecycleOwner, currentUser)
    }

    override fun clickListeners() {
        super.clickListeners()

        binding.fragmentProfileButtonLogout.setOnClickListener {
            viewModel.logout()
            navigator.resetAll()
            mainNavigate(LoginFragment.newInstance())
        }

        binding.fragmentProfileNotificationSwitch.setOnCheckedChangeListener { compoundButton, b ->
            viewModel.setAllowNotification(b)
        }

    }


    private val currentUser = Observer<FirebaseUser> {
        binding.data = it
    }


    companion object {
        fun newInstance() = ProfileFragment()
    }
}
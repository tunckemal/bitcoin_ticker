package com.android.bitcointicker.presentation.main

import android.os.Bundle
import com.android.bitcointicker.R
import com.android.bitcointicker.core.ui.BaseActivity
import com.android.bitcointicker.data.os.workmanager.BackgroundWorker
import com.android.bitcointicker.databinding.ActivityMainBinding
import com.android.bitcointicker.presentation.splash.SplashFragment
import com.android.easynav.src.FragmentController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class MainActivity : BaseActivity<MainVM, ActivityMainBinding>() {

    override val layoutRes = R.layout.activity_main
    override fun getViewModelClass() = MainVM::class.java

    /**
     * Uygulamalarda sayfalar arası geçiş kolaylığı ve esneklik açısından One Activity Multiple Fragment yapısını kullanıyorum
     * Bunun için Android Navigation component kullanılabilir
     * Ben burda kullanım açısından kolay olduğunu düşündüğim ve stackde aynı fragmentlerin duplicate olması engellemek için yaptıgım
     * kendi kütüphanemi kullanmayı tercih ettim
     *
     * https://github.com/kemaltunc/EasyController
     *
     * */

    val navigator = FragmentController(this, R.id.main_container)

    override fun initUI(savedInstanceState: Bundle?) {

        BackgroundWorker.schedule(this)

        navigator.apply {
            init(savedInstanceState)
            startFragment(SplashFragment.newInstance()) {
                history = false
            }
        }


    }

    override fun onSaveInstanceState(outState: Bundle) {
        navigator.saveState(outState)
        super.onSaveInstanceState(outState)
    }

}
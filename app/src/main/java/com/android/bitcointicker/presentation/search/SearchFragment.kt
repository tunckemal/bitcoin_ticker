package com.android.bitcointicker.presentation.search

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.widget.SearchView
import com.android.bitcointicker.R
import com.android.bitcointicker.core.Observe
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentSearchBinding
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.presentation.adapter.CoinAdapter
import com.android.bitcointicker.presentation.detail.DetailFragment
import com.android.bitcointicker.utility.LiveData.EventObserver
import com.android.bitcointicker.utility.constants.BundleConstants
import com.android.bitcointicker.utility.extension.createDialog
import com.android.bitcointicker.utility.extension.vertical
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_information_search.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class SearchFragment : BaseFragment<SearchVM, FragmentSearchBinding>() {
    override val layoutRes = R.layout.fragment_search
    override fun getViewModelClass() = SearchVM::class.java

    private val adapter by lazy { CoinAdapter() }

    private var query: String = ""


    override fun define() {
        super.define()

        query = arguments?.getString(BundleConstants.QUERY, "") ?: ""
    }

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)

        createAdapter()


        viewModel.checkLocalDb { result ->

            if (result) {
                viewModel.getList()
            } else {
                showDialog()
            }
        }

        binding.fragmentSearchSearchview.setOnQueryTextListener(textChangeListener)
        setFocus()
    }

    override fun clickListeners() {
        super.clickListeners()

        adapter.onItemClick {
            hideKeyboard()
            if (it is CoinEntity) {
                mainNavigate(DetailFragment.newInstance(it.id))
            }
        }
    }

    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.search.observe(viewLifecycleOwner, search)
        viewModel.loadedData.observe(viewLifecycleOwner, loadedData)
    }

    private fun createAdapter() {
        binding.fragmentSearchRecyclerview.vertical(adapter, bottomDetect = {

        })
    }

    private val loadedData = EventObserver<Boolean> {
        if (query.isNotEmpty()) {
            binding.fragmentSearchSearchview.setQuery(query, false)
        }
    }

    private fun showDialog() {
        context?.let {
            val d = Dialog(it).createDialog(R.layout.dialog_information_search, view = { view ->
                view.dialog_button.setOnClickListener {
                    view.dismiss()
                }
            })

            d.setOnDismissListener {
                viewModel.getList()
            }
        }
    }


    private val search = Observe<List<CoinEntity>> {
        it.handle(success = { coins ->
            adapter.items = coins
        })
    }

    private fun setFocus() {
        Handler(Looper.getMainLooper()).postDelayed({
            binding.fragmentSearchSearchview.apply {
                showKeyboard()
                requestFocus()
            }
        }, 200L)
    }

    private val textChangeListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            if (newText != null) {
                viewModel.search(newText)
            }
            return false
        }

    }


    companion object {
        fun newInstance(query: String? = ""): SearchFragment {

            val searchFragment = SearchFragment()

            val bundle = Bundle()
            bundle.putString(BundleConstants.QUERY, query)

            searchFragment.arguments = bundle
            return searchFragment

        }
    }
}
package com.android.bitcointicker.presentation.favorite

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.android.bitcointicker.core.LiveWithDataState
import com.android.bitcointicker.core.ObserveData
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.domain.usecase.CoinUseCase
import com.android.bitcointicker.domain.usecase.FavoriteUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class FavoriteVM @ViewModelInject constructor(private val favoriteUseCase: FavoriteUseCase) :
    BaseViewModel() {

    private val _favorites = ObserveData<List<CoinEntity>?>()

    val favorites: LiveWithDataState<List<CoinEntity>?>
        get() = _favorites


    fun getFavorites() {
        favoriteUseCase.getFavorite {
            _favorites.value=it
        }
    }

}
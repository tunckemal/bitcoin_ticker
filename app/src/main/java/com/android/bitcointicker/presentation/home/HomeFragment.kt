package com.android.bitcointicker.presentation.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.Menu
import android.view.MenuInflater
import androidx.appcompat.app.AppCompatActivity
import com.android.bitcointicker.R
import com.android.bitcointicker.core.Observe
import com.android.bitcointicker.core.adapter.RecyclerItem
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentHomeBinding
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.presentation.adapter.CoinAdapter
import com.android.bitcointicker.presentation.adapter.TitleItem
import com.android.bitcointicker.presentation.detail.DetailFragment
import com.android.bitcointicker.presentation.search.SearchFragment
import com.android.bitcointicker.utility.extension.gone
import com.android.bitcointicker.utility.extension.show
import com.android.bitcointicker.utility.extension.vertical
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class HomeFragment : BaseFragment<HomeVM, FragmentHomeBinding>() {
    override val layoutRes = R.layout.fragment_home
    override fun getViewModelClass() = HomeVM::class.java

    private val REQUEST_VOICE_CODE: Int = 1234

    private val adapter by lazy { CoinAdapter() }


    lateinit var homeItem: ArrayList<RecyclerItem>

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)


        initToolbar()
        createAdapter()

        binding.homeRefreshLayout.setOnRefreshListener {
            viewModel.getTrends()
        }


    }

    override fun runOnce() {
        super.runOnce()

        viewModel.getTrends()
    }


    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.trends.observe(viewLifecycleOwner, trends)

    }


    private val trends = Observe<List<CoinEntity>> {
        it.handle(
            loading = {
                binding.fragmentHomeProgress.show()
            },
            success = { data ->
                homeItem = ArrayList()
                homeItem.apply {
                    add(TitleItem(getString(R.string.title_trends)))
                    addAll(data)
                    adapter.items = this
                }
            }, error = {
                binding.fragmentHomeErrorMessage.show()
            },
            hideLoading = {
                binding.fragmentHomeProgress.gone()
                binding.homeRefreshLayout.isRefreshing = false
            })
    }


    private fun createAdapter() {
        binding.fragmentHomeRecyclerview.vertical(adapter, bottomDetect = {

        })
    }

    private fun initToolbar() {
        (activity as AppCompatActivity).apply {
            setSupportActionBar(binding.tabHeader.tabToolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun clickListeners() {
        super.clickListeners()

        binding.tabHeader.searchTv.setOnClickListener {
            mainNavigate(SearchFragment.newInstance())
        }


        adapter.onItemClick {
            if (it is CoinEntity) {
                mainNavigate(DetailFragment.newInstance(it.id))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        menu.findItem(R.id.search_view).setOnMenuItemClickListener {
            startVoiceRecognitionActivity()
            return@setOnMenuItemClickListener true
        }

        super.onCreateOptionsMenu(menu, inflater)
    }


    private fun startVoiceRecognitionActivity() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.information_listening))
        startActivityForResult(intent, REQUEST_VOICE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_VOICE_CODE && resultCode == Activity.RESULT_OK) {
            val matches: ArrayList<String> =
                data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS) as ArrayList<String>

            if (matches.isNotEmpty()) {
                val query = matches[0]
                mainNavigate(SearchFragment.newInstance(query))
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        fun newInstance() = HomeFragment()
    }


}

package com.android.bitcointicker.presentation.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.bitcointicker.core.ObserveData
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.domain.usecase.AccountUseCase
import com.android.bitcointicker.domain.usecase.ServerUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class SplashVM @ViewModelInject constructor(
    private val serverUseCase: ServerUseCase,
    private val accountUseCase: AccountUseCase
) : BaseViewModel() {


    val checkServer = ObserveData<String>()
    val isLoggedIn = MutableLiveData<Boolean>()

    fun startApp() {
        execute {
            serverUseCase.checkServer()
        }.onEach {
            checkServer.value = it
        }.launchIn(viewModelScope)
    }

    fun isLoggedIn() {
        isLoggedIn.value = accountUseCase.isLoggedIn()
    }
}
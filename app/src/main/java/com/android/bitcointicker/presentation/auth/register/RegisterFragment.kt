package com.android.bitcointicker.presentation.auth.register

import android.os.Bundle
import com.android.bitcointicker.R
import com.android.bitcointicker.core.Observe
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentRegisterBinding
import com.android.bitcointicker.presentation.auth.AccountVM
import com.android.bitcointicker.presentation.tab.TabFragment
import com.android.bitcointicker.utility.LiveData.EventObserver
import com.android.bitcointicker.utility.enums.FormError
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class RegisterFragment : BaseFragment<AccountVM, FragmentRegisterBinding>() {

    override val layoutRes = R.layout.fragment_register
    override fun getViewModelClass() = AccountVM::class.java

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)

    }

    override fun clickListeners() {
        super.clickListeners()

        binding.fragmentRegisterBtn.setOnClickListener {
            viewModel.register(
                binding.fragmentRegisterEmailEd.text,
                binding.fragmentRegisterPasswordEd.text,
                binding.fragmentRegisterRepasswordEd.text
            )
        }
    }

    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.formError.observe(viewLifecycleOwner, formError)
        viewModel.firebaseUser.observe(viewLifecycleOwner, firebaseUser)
    }

    private val formError = EventObserver<FormError> {
        when (it) {
            FormError.INVALID_MAIL -> {
                showMessage(getString(R.string.invalid_email))
            }
            FormError.SHORT_PASSWORD -> {
                showMessage(getString(R.string.invalid_password))
            }
            FormError.NOT_MATCH_PASSWORD -> {
                showMessage(getString(R.string.passwords_not_match))
            }
            FormError.NOTHING -> {

            }
            else -> {
                showMessage(getString(R.string.cannot_be_black))
            }
        }
    }

    private val firebaseUser = Observe<FirebaseUser?> {
        it.handle(loading = {
            showLoading()
        }, success = {
            mainNavigate(TabFragment.newInstance()) {
                history = false
                clearHistory = true
            }
        }, error = { error ->
            showMessage(error.message ?: "")
        }, hideLoading = {
            hideLoading()
        })
    }

    companion object {
        fun newInstance() = RegisterFragment()
    }
}
package com.android.bitcointicker.presentation.auth

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.core.ObserveData
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.data.source.network.firebase.auth.FirebaseAccountImpl
import com.android.bitcointicker.domain.usecase.AccountUseCase
import com.android.bitcointicker.utility.LiveData.Event
import com.android.bitcointicker.utility.LiveData.event
import com.android.bitcointicker.utility.enums.FormError
import com.android.bitcointicker.utility.extension.regexMail
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class AccountVM @ViewModelInject constructor(private val accountUseCase: AccountUseCase) :
    BaseViewModel() {

    val formError = MutableLiveData<Event<FormError>>()
    val firebaseUser = ObserveData<FirebaseUser?>()

    fun login(email: String?, password: String?) {
        formError.value = when {
            email.isNullOrEmpty() -> FormError.MISSING_EMAIL
            !email.regexMail() -> FormError.INVALID_MAIL
            password.isNullOrEmpty() -> FormError.MISSING_PASSWORD
            password.length < 6 -> FormError.SHORT_PASSWORD
            else -> {

                val fieldMap = FieldMap()
                fieldMap[FirebaseAccountImpl.EMAIL] = email
                fieldMap[FirebaseAccountImpl.PASSWORD] = password


                accountUseCase.signIn(fieldMap) {
                    firebaseUser.value = it
                }

                FormError.NOTHING
            }

        }.event()

    }


    fun register(email: String?, password: String?, rePassword: String?) {
        formError.value = when {
            email.isNullOrEmpty() -> FormError.MISSING_EMAIL
            !email.regexMail() -> FormError.INVALID_MAIL
            password.isNullOrEmpty() -> FormError.MISSING_PASSWORD
            password.length < 6 -> FormError.SHORT_PASSWORD
            rePassword.isNullOrEmpty() -> FormError.MISSING_RE_PASSWORD
            rePassword.length < 6 -> FormError.SHORT_PASSWORD
            password != rePassword -> FormError.NOT_MATCH_PASSWORD
            else -> {

                val fieldMap = FieldMap()
                fieldMap[FirebaseAccountImpl.EMAIL] = email
                fieldMap[FirebaseAccountImpl.PASSWORD] = password


                accountUseCase.createNewAccount(fieldMap) {
                    firebaseUser.value = it
                }

                FormError.NOTHING
            }

        }.event()

    }

}
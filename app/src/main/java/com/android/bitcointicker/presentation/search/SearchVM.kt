package com.android.bitcointicker.presentation.search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.bitcointicker.core.LiveWithDataState
import com.android.bitcointicker.core.ObserveData
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.domain.usecase.CoinUseCase
import com.android.bitcointicker.utility.LiveData.Event
import com.android.bitcointicker.utility.LiveData.event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class SearchVM @ViewModelInject constructor(private val coinUseCase: CoinUseCase) :
    BaseViewModel() {


    private val _search = ObserveData<List<CoinEntity>>()

    val search: LiveWithDataState<List<CoinEntity>>
        get() = _search


    val loadedData = MutableLiveData<Event<Boolean>>()

    fun search(query: String) {
        execute {
            coinUseCase.search(query)
        }.onEach {
            _search.value = it
        }.launchIn(viewModelScope)
    }

    fun getList() {
        execute { coinUseCase.getList() }.onEach {
            it.handle(
                loading = {
                    showLoading.value = true.event()
                }, hideLoading = {
                    hideLoading.value = true.event()
                },
                success = {
                    loadedData.value = true.event()
                }
            )
        }.launchIn(viewModelScope)
    }

    fun checkLocalDb(result: (result: Boolean) -> Unit) {
        viewModelScope.launch {
            result(coinUseCase.checkLocalDb())
        }
    }

}
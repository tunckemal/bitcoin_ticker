package com.android.bitcointicker.presentation.favorite

import android.os.Bundle
import com.android.bitcointicker.R
import com.android.bitcointicker.core.Observe
import com.android.bitcointicker.core.adapter.RecyclerItem
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentFavoriteBinding
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.presentation.adapter.CoinAdapter
import com.android.bitcointicker.presentation.adapter.TitleItem
import com.android.bitcointicker.presentation.detail.DetailFragment
import com.android.bitcointicker.utility.extension.gone
import com.android.bitcointicker.utility.extension.show
import com.android.bitcointicker.utility.extension.vertical
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class FavoriteFragment : BaseFragment<FavoriteVM, FragmentFavoriteBinding>() {

    override val layoutRes = R.layout.fragment_favorite
    override fun getViewModelClass() = FavoriteVM::class.java


    private val adapter by lazy { CoinAdapter() }

    lateinit var favoriteItem: ArrayList<RecyclerItem>

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)

        createAdapter()

        viewModel.getFavorites()
    }


    private fun createAdapter() {
        binding.fragmentFavoriteRecyclerview.vertical(adapter, bottomDetect = {

        })
    }

    override fun subscribeObservers() {
        super.subscribeObservers()
        viewModel.favorites.observe(viewLifecycleOwner, favorites)

    }

    override fun clickListeners() {
        super.clickListeners()

        adapter.onItemClick {
            if (it is CoinEntity) {
                mainNavigate(DetailFragment.newInstance(it.id))
            }
        }
    }


    private val favorites = Observe<List<CoinEntity>?> {
        it.handle(
            loading = {
                binding.fragmentSearchProgress.show()
            },
            success = { data ->
                favoriteItem = ArrayList()
                favoriteItem.apply {
                    add(TitleItem(getString(R.string.title_favorites)))
                    addAll(data ?: emptyList())
                    adapter.items = this
                }
            }, error = {
                binding.fragmentFavoriteErrorMessage.show()
            },
            hideLoading = {
                binding.fragmentSearchProgress.gone()
            })
    }

    companion object {
        fun newInstance() = FavoriteFragment()
    }
}
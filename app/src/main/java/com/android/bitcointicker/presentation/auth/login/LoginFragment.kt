package com.android.bitcointicker.presentation.auth.login

import android.os.Bundle
import com.android.bitcointicker.R
import com.android.bitcointicker.core.Observe
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentLoginBinding
import com.android.bitcointicker.presentation.auth.AccountVM
import com.android.bitcointicker.presentation.auth.register.RegisterFragment
import com.android.bitcointicker.presentation.tab.TabFragment
import com.android.bitcointicker.utility.LiveData.EventObserver
import com.android.bitcointicker.utility.enums.FormError
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class LoginFragment : BaseFragment<AccountVM, FragmentLoginBinding>() {

    override val layoutRes = R.layout.fragment_login
    override fun getViewModelClass() = AccountVM::class.java

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)

    }

    override fun clickListeners() {
        super.clickListeners()

        binding.fragmentLoginBtn.setOnClickListener {
            viewModel.login(binding.fragmentLoginEmailEd.text, binding.fragmentLoginPasswordEd.text)
        }

        binding.registerTv.setOnClickListener {
            mainNavigate(RegisterFragment.newInstance())
        }
    }

    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.formError.observe(viewLifecycleOwner, formError)
        viewModel.firebaseUser.observe(viewLifecycleOwner, firebaseUser)
    }

    private val formError = EventObserver<FormError> {
        when (it) {
            FormError.INVALID_MAIL -> {
                showMessage(getString(R.string.invalid_email))
            }
            FormError.SHORT_PASSWORD -> {
                showMessage(getString(R.string.invalid_password))
            }
            FormError.NOTHING -> {

            }
            else -> {
                showMessage(getString(R.string.cannot_be_black))
            }
        }
    }

    private val firebaseUser = Observe<FirebaseUser?> {
        it.handle(loading = {
            showLoading()
        }, success = {
            mainNavigate(TabFragment.newInstance()) {
                clearHistory = true
                history = false
            }
        }, error = { error ->
            showMessage(error.message ?: "")
        }, hideLoading = {
            hideLoading()
        })
    }

    companion object {
        fun newInstance() = LoginFragment()
    }
}
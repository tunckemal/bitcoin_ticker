package com.android.bitcointicker.presentation.main

import androidx.hilt.lifecycle.ViewModelInject
import com.android.bitcointicker.core.ui.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class MainVM @ViewModelInject constructor() : BaseViewModel() {

}
package com.android.bitcointicker.presentation.profile

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.data.source.local.pref.PrefHelper
import com.android.bitcointicker.domain.usecase.AccountUseCase
import com.android.bitcointicker.utility.LiveData.default
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class ProfileVM @ViewModelInject constructor(
    private val accountUseCase: AccountUseCase,
    private val prefHelper: PrefHelper
) : BaseViewModel() {

    val user = MutableLiveData<FirebaseUser>()

    fun getCurrentUser() {
        user.value = accountUseCase.getCurrentUser()
    }


    fun getAllowNotification() = prefHelper.getAllowNotification()

    fun setAllowNotification(bool: Boolean) = prefHelper.setAllowNotification(bool)

    fun logout() {
        accountUseCase.logout()
    }
}
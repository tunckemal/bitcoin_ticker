package com.android.bitcointicker.presentation.splash

import android.os.Bundle
import androidx.lifecycle.Observer
import com.android.bitcointicker.R
import com.android.bitcointicker.core.Observe
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.core.ui.BaseFragment
import com.android.bitcointicker.databinding.FragmentSplashBinding
import com.android.bitcointicker.presentation.auth.login.LoginFragment
import com.android.bitcointicker.presentation.tab.TabFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class SplashFragment : BaseFragment<SplashVM, FragmentSplashBinding>() {

    override val layoutRes = R.layout.fragment_splash
    override fun getViewModelClass() = SplashVM::class.java

    override fun initUI(savedInstanceState: Bundle?) {
        super.initUI(savedInstanceState)

        viewModel.startApp()
    }


    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.checkServer.observe(viewLifecycleOwner, checkServer)
        viewModel.isLoggedIn.observe(viewLifecycleOwner, isLoggedIn)

    }

    private val checkServer = Observe<String> {
        it.handle(success = {
            viewModel.isLoggedIn()
        }, error = {

        })
    }

    private val isLoggedIn = Observer<Boolean> {
        if (it) {
            mainNavigate(TabFragment.newInstance()) {
                history = false
            }
        } else {
            mainNavigate(LoginFragment.newInstance())
        }
    }


    companion object {
        fun newInstance() = SplashFragment()
    }


}

package com.android.bitcointicker.presentation.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.android.bitcointicker.core.LiveWithDataState
import com.android.bitcointicker.core.ObserveData
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.domain.usecase.CoinUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class HomeVM @ViewModelInject constructor(
    private val coinUseCase: CoinUseCase
) : BaseViewModel() {

    private val _trends = ObserveData<List<CoinEntity>>()

    val trends: LiveWithDataState<List<CoinEntity>>
        get() = _trends

    fun getTrends() {
        execute { coinUseCase.getTrends() }.onEach {
            _trends.value = it
        }.launchIn(viewModelScope)

    }

}
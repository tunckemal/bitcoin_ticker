package com.android.bitcointicker.presentation.tab

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentController
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.android.bitcointicker.core.ui.BaseViewModel
import com.android.bitcointicker.presentation.favorite.FavoriteFragment
import com.android.bitcointicker.presentation.home.HomeFragment
import com.android.bitcointicker.presentation.profile.ProfileFragment
import com.android.bitcointicker.utility.LiveData.Event
import com.android.bitcointicker.utility.LiveData.event
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class TabVM @ViewModelInject constructor() : BaseViewModel() {








}
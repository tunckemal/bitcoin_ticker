package com.android.bitcointicker.utility.enums

enum class FormError {
    MISSING_EMAIL,
    MISSING_PASSWORD,
    MISSING_RE_PASSWORD,
    NOT_MATCH_PASSWORD,
    INVALID_MAIL,
    SHORT_PASSWORD,
    NOTHING
}
package com.android.bitcointicker.utility.enums


class AdapterEnum {

    enum class CoinEnum(var type: Int) {
        TITLE(1),
        COIN(2),
        SEARCH(3)
    }

}
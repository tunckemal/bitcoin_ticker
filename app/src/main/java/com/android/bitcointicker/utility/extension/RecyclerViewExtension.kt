package com.android.bitcointicker.utility.extension

import android.widget.LinearLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.android.bitcointicker.utility.EndlessOnScrollListener
import com.android.bitcointicker.utility.PreCachingLayoutManager

@JvmOverloads
fun RecyclerView.vertical(
    adapter: RecyclerView.Adapter<*>,
    layoutManager: RecyclerView.LayoutManager? = null,
    bottomDetect: () -> Unit = {}
) {
    this.run {
        this.layoutManager =
            layoutManager ?:   // LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
                    PreCachingLayoutManager(this.context, LinearLayout.VERTICAL, false)

        this.adapter = adapter


        setHasFixedSize(true)
        setItemViewCacheSize(30)
    }

    this.addOnScrollListener(object : EndlessOnScrollListener() {
        override fun onScrolledToEnd() {
            bottomDetect()
        }
    })
}


@JvmOverloads
fun RecyclerView.horizantal(
    adapter: RecyclerView.Adapter<*>,
    layoutManager: RecyclerView.LayoutManager? = null,
    snapHelper: SnapHelper? = null
) {
    this.run {
        this.layoutManager =
            layoutManager ?: LinearLayoutManager(this.context, RecyclerView.HORIZONTAL, false)
                  //  PreCachingLayoutManager(this.context, LinearLayout.HORIZONTAL, false)
        this.adapter = adapter

        setHasFixedSize(true)
        setItemViewCacheSize(30)

        onFlingListener = null
    }
    try {
        snapHelper?.attachToRecyclerView(this)
    } catch (e: IllegalStateException) {
        e.printStackTrace()
    }


}

@JvmOverloads
fun RecyclerView.grid(
    adapter: RecyclerView.Adapter<*>,
    layoutManager: RecyclerView.LayoutManager? = null,
    column: Int? = 2
) {
    this.run {
        this.layoutManager = layoutManager ?: GridLayoutManager(context, column ?: 1)
        this.adapter = adapter

        setHasFixedSize(true)
        setItemViewCacheSize(30)
    }
}


@JvmOverloads
fun RecyclerView.position(position: (Int?) -> Unit) {


    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)

            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                val pos =
                    (this@position.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                position(pos)
            }
        }
    })
}
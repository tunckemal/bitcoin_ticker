package com.android.bitcointicker.utility.constants

class BundleConstants {

    companion object {
        const val COIN_ID = "coin_id"
        const val QUERY = "query"
    }
}

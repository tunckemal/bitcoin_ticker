package com.android.bitcointicker.utility.extension

import android.app.Dialog
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView

val Int.px: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.margin(top: Int? = null, left: Int? = null, bottom: Int? = null, right: Int? = null) {

    val param = this.layoutParams as ViewGroup.MarginLayoutParams

    top?.let {
        param.topMargin = it
    }
    left?.let {
        param.leftMargin = it
    }
    bottom?.let {
        param.bottomMargin = it
    }
    right?.let {
        param.rightMargin = it
    }

    this.layoutParams = param

}

fun TextView.centerLine() {
    this.paintFlags = this.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}

fun Dialog.createDialog(
    layoutId: Int,
    cancelable: Boolean = true,
    view: (view: Dialog) -> Unit
): Dialog {

    val dialog = this
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    dialog.setContentView(layoutId)
    dialog.setCanceledOnTouchOutside(cancelable)

    val window = dialog.window
    window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

    dialog.show()
    view(dialog)

    return dialog

}

fun View.fieldFocusListener(focus: (view: View) -> Unit) {
    this.setOnFocusChangeListener { v, hasFocus ->
        if (hasFocus) {
            focus(this)
        }
    }
}

fun EditText.onSubmit(func: () -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->

        if (actionId == EditorInfo.IME_ACTION_DONE) {
            func()
        }

        true

    }
}

package com.android.bitcointicker.utility.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.android.bitcointicker.utility.extension.loadWithCache


@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String?) {
    view.loadWithCache(url ?: "")
}
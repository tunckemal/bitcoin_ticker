package com.android.bitcointicker.utility.extension

import android.util.Log
import java.util.regex.Pattern

fun String.Logd(message: String) {
    Log.d(this, message)
}

fun String.regexMail(): Boolean {
    val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    return EMAIL_ADDRESS_PATTERN.matcher(this).matches()

}
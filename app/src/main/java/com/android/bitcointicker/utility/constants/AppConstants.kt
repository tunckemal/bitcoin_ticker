package com.android.bitcointicker.utility.constants


class AppConstants {
    companion object {
        const val COLLECTION_NAME = "app"
        const val SPLASH_DELAY = 1000L
        const val SEARCH_DELAY = 500L

    }
}



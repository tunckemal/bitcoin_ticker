package com.android.bitcointicker.utility

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import com.android.bitcointicker.R
import kotlinx.android.synthetic.main.loading_view.*

class Loading(private val context: Context) {
    fun showLoadingDialog(): Dialog {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.loading_view)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
        dialog.loading.show()

        return dialog
    }
}

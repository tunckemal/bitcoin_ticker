package com.android.bitcointicker.utility.databinding

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("onClick")
fun onClick(view: View, onClick: () -> Unit) {
    view.setOnClickListener {
        onClick()
    }
}
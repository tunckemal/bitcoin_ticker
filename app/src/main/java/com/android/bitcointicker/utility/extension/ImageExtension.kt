package com.android.bitcointicker.utility.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy


fun ImageView.loadWithCache(path: String) {
    /*  val circularProgressDrawable = CircularProgressDrawable(this.context)
      circularProgressDrawable.strokeWidth = 5f
      circularProgressDrawable.centerRadius = 30f
  */
    //circularProgressDrawable.start()
    Glide.with(this.context).load(path).diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}


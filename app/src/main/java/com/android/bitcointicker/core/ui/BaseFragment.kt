package com.android.bitcointicker.core.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.bitcointicker.presentation.main.MainActivity
import com.android.bitcointicker.utility.LiveData.EventObserver
import com.android.bitcointicker.utility.extension.Logd
import com.android.easynav.src.FragmentController
import com.android.easynav.src.FragmentOption
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
abstract class BaseFragment<ViewModel : BaseViewModel, ViewBinding : ViewDataBinding> : Fragment() {

    @get:LayoutRes
    protected abstract val layoutRes: Int
    protected abstract fun getViewModelClass(): Class<ViewModel>

    private var baseActivity: BaseActivity<*, *>? = null


    protected val viewModel: ViewModel by lazy { ViewModelProvider(this).get(getViewModelClass()) }


    lateinit var binding: ViewBinding


    lateinit var navigator: FragmentController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        define()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        binding.lifecycleOwner = viewLifecycleOwner


        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initUI(savedInstanceState)
        subscribeObservers()
        clickListeners()


    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            this.baseActivity = context
        }

        if (context is MainActivity) {
            this.navigator = context.navigator
        }

    }


    open fun define() {}
    open fun initUI(savedInstanceState: Bundle?) {}

    open fun clickListeners() {}

    open fun reCreated() {}

    open fun subscribeObservers() {

        viewModel.startScreen.observe(viewLifecycleOwner, Observer { result ->
            if (!result) {
                viewModel.setStartScreen()
                runOnce()
            }
        })

        viewModel.errorHandle.observe(viewLifecycleOwner, EventObserver {
            it.message?.let { message ->
                "ErrorHandler".Logd(message)
                showMessage(message)
            }
        })


        viewModel.showLoading.observe(viewLifecycleOwner, EventObserver {
            showLoading()
        })


        viewModel.hideLoading.observe(viewLifecycleOwner, EventObserver {
            hideLoading()
        })


    }

    open fun runOnce() {}


    open fun showMessage(message: String) {
        baseActivity?.showMessage(message)
    }

    open fun showMessage(messageId: Int) {
        baseActivity?.showMessage(messageId)
    }

    open fun showLoading() {
        baseActivity?.showLoading()
    }

    open fun hideLoading() {
        baseActivity?.hideLoading()
    }

    open fun showSnackBar(
        rootView: View,
        message: String,
        actionMessage: String,
        clickActionButton: () -> Unit
    ) {
        val snackbar = Snackbar.make(
            rootView,
            message, Snackbar.LENGTH_LONG
        )

        snackbar.setAction(
            actionMessage
        ) {
            snackbar.dismiss()
            clickActionButton()
        }

        snackbar.show()
    }

    open fun mainNavigate(
        fragment: Fragment,
        block: FragmentOption.Builder.() -> Unit = {}
    ) {
        navigator.mainNavigate(FragmentOption.build(fragment, block))
    }

    open fun childNavigate(
        fragment: Fragment,
        block: FragmentOption.Builder.() -> Unit = {}
    ) {
        navigator.childNavigate(FragmentOption.build(fragment, block))
    }

    fun showKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

}
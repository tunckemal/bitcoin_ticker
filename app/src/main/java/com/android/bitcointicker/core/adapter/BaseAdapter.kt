package com.android.bitcointicker.core.adapter

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import kotlin.properties.Delegates


abstract class BaseAdapter<Data : RecyclerItem> :
    ListAdapter<Data, BaseViewHolder<Data, *>>(DiffCallback<Data>()) {


    private var onItemClick: ((Data) -> Unit) = {}

    private var onViewClick: ((Data, View) -> Unit) = { _, _ -> }


    var items: List<Data> by Delegates.observable(emptyList()) { prop, old, new ->
        this.submitList(new)

    }

    abstract override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Data, *>

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Data, *>, position: Int) {
        holder.setOnViewClick(onViewClick).bindItem(getItem(position), onItemClick)
    }

    fun onItemClick(onClick: ((Data) -> Unit)): BaseAdapter<Data> {
        this.onItemClick = onClick
        return this
    }


    fun onViewClick(onClick: ((Data, View) -> Unit)): BaseAdapter<Data> {
        this.onViewClick = onClick
        return this
    }




    class DiffCallback<Data : RecyclerItem> : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data) =
            oldItem.compareId == newItem.compareId

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Data, newItem: Data) = oldItem == newItem
    }

}
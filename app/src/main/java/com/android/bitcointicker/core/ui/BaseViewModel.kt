package com.android.bitcointicker.core.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.data.source.network.exception.ApiErrorHandle
import com.android.bitcointicker.data.source.network.exception.ErrorModel
import com.android.bitcointicker.utility.LiveData.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@ExperimentalCoroutinesApi
abstract class BaseViewModel : ViewModel() {


    val startScreen = MutableLiveData<Boolean>()

    init {
        startScreen.value = false
    }

    fun setStartScreen() {
        startScreen.value = true
    }

    val errorHandle =
        MutableLiveData<Event<ErrorModel>>()
    val showLoading =
        MutableLiveData<Event<Boolean>>()
    val hideLoading =
        MutableLiveData<Event<Boolean>>()


    fun <T> execute(call: suspend () -> T): Flow<DataState<T>> = flow {
        emit(DataState.Loading)

        try {
            val result = call.invoke()
            emit(DataState.Success(result))

        } catch (e: Exception) {
            val error = ApiErrorHandle().traceErrorException(e)
            errorHandle.value = Event(error)
            emit(DataState.Error(error))
            hideLoading.value = Event(true)
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}
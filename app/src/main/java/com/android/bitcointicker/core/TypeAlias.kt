package com.android.bitcointicker.core

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.android.bitcointicker.utility.LiveData.Event
import kotlinx.coroutines.flow.Flow

typealias FieldMap = HashMap<String, Any>

typealias FlowEntity<T> = Flow<DataState<T>>

typealias SingleMutableEvent<T> = MutableLiveData<Event<DataState<T>>>
typealias SingleLiveData<T> = LiveData<Event<DataState<T>>>


typealias ObserveData<T> = MutableLiveData<DataState<T>>
typealias LiveWithDataState<T> = LiveData<DataState<T>>


typealias Observe<T> = Observer<DataState<T>>
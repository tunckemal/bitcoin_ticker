package com.android.bitcointicker.core.ui


import android.app.Dialog
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.android.bitcointicker.utility.Loading


abstract class BaseActivity<ViewModel : BaseViewModel, ViewBinding : ViewDataBinding> :
    AppCompatActivity() {

    @get:LayoutRes
    protected abstract val layoutRes: Int

    protected abstract fun getViewModelClass(): Class<ViewModel>


    protected val viewModel: ViewModel by lazy { ViewModelProvider(this).get(getViewModelClass()) }

    private lateinit var loading: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)

        initUI(savedInstanceState)
    }

    open fun initUI(savedInstanceState: Bundle?) {}
    open fun subscribeObservers() {}

    open fun showMessage(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    open fun showMessage(messageId: Int) {
        Toast.makeText(applicationContext, getString(messageId), Toast.LENGTH_SHORT).show()
    }

    open fun showLoading() {
        if (!this::loading.isInitialized) {
            loading = Loading(this).showLoadingDialog()
        } else {
            if (!loading.isShowing) {
                loading.show()
            }
        }
    }

    open fun hideLoading() {
        if (this::loading.isInitialized) {
            loading.dismiss()
            loading.cancel()
        }
    }

}
package com.android.bitcointicker.data.source.network.firebase.cloud.base

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.QuerySnapshot

interface FirebaseCloud {

    fun ref(): CollectionReference

    fun getUserId(): String?

    fun delete(ref: CollectionReference, id: String, ok: () -> Unit)

    fun deleteAll(ref: CollectionReference, ok: () -> Unit)

    fun get(ref: CollectionReference, data: (data: DataState<QuerySnapshot?>) -> Unit)
    fun save(
        ref: CollectionReference,
        fieldMap: FieldMap,
        data: (data: DataState<Boolean?>) -> Unit
    )
}
package com.android.bitcointicker.data.source.network.firebase.auth

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.data.source.network.exception.ex
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class FirebaseAccountImpl : FirebaseAccount {

    private val mAuth = FirebaseAuth.getInstance()

    override fun getCurrentUser(): FirebaseUser? {
        return mAuth.currentUser
    }

    override fun getUserId(): String? {
        return mAuth.currentUser?.uid
    }

    override fun isLoggedIn() = mAuth.currentUser != null

    override fun createNewAccount(
        fieldMap: FieldMap,
        data: (data: DataState<FirebaseUser?>) -> Unit
    ) {

        data(DataState.Loading)

        val email = fieldMap[EMAIL].toString()
        val password = fieldMap[PASSWORD].toString()

        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    data(DataState.Success(mAuth.currentUser))
                } else {
                    data(DataState.Error(task.exception.ex()))
                }
            }
    }

    override fun signIn(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit) {
        data(DataState.Loading)
        val email = fieldMap[EMAIL].toString()
        val password = fieldMap[PASSWORD].toString()

        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    data(DataState.Success(mAuth.currentUser))
                } else {
                    data(DataState.Error(task.exception.ex()))
                }
            }
    }

    override fun logout() {
        mAuth.signOut()
    }

    companion object {
        const val EMAIL = "email"
        const val PASSWORD = "password"
    }

}
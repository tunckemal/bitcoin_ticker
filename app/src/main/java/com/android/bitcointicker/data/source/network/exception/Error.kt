package com.android.bitcointicker.data.source.network.exception


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Error(
    @Json(name = "error")
    var error: String?,
    @Json(name = "status")
    var status: Int?
)
package com.android.bitcointicker.data.repository

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.data.source.network.firebase.auth.FirebaseAccount
import com.android.bitcointicker.domain.repository.AccountRepository
import com.google.firebase.auth.FirebaseUser

class AccountRepositoryImpl(private val firebaseAccount: FirebaseAccount) : AccountRepository {
    override fun getCurrentUser(): FirebaseUser? {
        return firebaseAccount.getCurrentUser()
    }

    override fun isLoggedIn(): Boolean {
        return firebaseAccount.isLoggedIn()
    }

    override fun createNewAccount(
        fieldMap: FieldMap,
        data: (data: DataState<FirebaseUser?>) -> Unit
    ) {
        return firebaseAccount.createNewAccount(fieldMap, data)
    }

    override fun signIn(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit) {
        return firebaseAccount.signIn(fieldMap, data)
    }

    override fun logout(){
        firebaseAccount.logout()
    }
}
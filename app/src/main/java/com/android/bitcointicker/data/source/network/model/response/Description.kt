package com.android.bitcointicker.data.source.network.model.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Description(
    @Json(name = "ar")
    var ar: String?,
    @Json(name = "de")
    var de: String?,
    @Json(name = "en")
    var en: String?,
    @Json(name = "es")
    var es: String?,
    @Json(name = "fr")
    var fr: String?,
    @Json(name = "hu")
    var hu: String?,
    @Json(name = "id")
    var id: String?,
    @Json(name = "it")
    var `it`: String?,
    @Json(name = "ja")
    var ja: String?,
    @Json(name = "ko")
    var ko: String?,
    @Json(name = "nl")
    var nl: String?,
    @Json(name = "pl")
    var pl: String?,
    @Json(name = "pt")
    var pt: String?,
    @Json(name = "ro")
    var ro: String?,
    @Json(name = "ru")
    var ru: String?,
    @Json(name = "sv")
    var sv: String?,
    @Json(name = "th")
    var th: String?,
    @Json(name = "tr")
    var tr: String?,
    @Json(name = "vi")
    var vi: String?,
    @Json(name = "zh")
    var zh: String?,
    @Json(name = "zh-tw")
    var zhTw: String?
)
package com.android.bitcointicker.data.os.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.android.bitcointicker.R
import com.android.bitcointicker.data.source.local.pref.PrefHelper

class Notification(
    private val prefHelper: PrefHelper
) {

    fun showNotification(context: Context, title: String, message: String) {
        if (prefHelper.getAllowNotification()) {
            val channelId = "default"
            val channelName = "Channel Name"

            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel =
                    NotificationChannel(
                        channelId,
                        channelName,
                        NotificationManager.IMPORTANCE_DEFAULT
                    )
                notificationManager.createNotificationChannel(channel)
            }

            val notification: NotificationCompat.Builder =
                NotificationCompat.Builder(context, channelId)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_launcher_background)

            notificationManager.notify(1, notification.build())
        }
    }
}
package com.android.bitcointicker.data.source.network.service

import com.android.bitcointicker.data.source.network.model.response.Item
import com.android.bitcointicker.data.source.network.model.response.MainCoinResponse
import com.android.bitcointicker.data.source.network.model.response.TrendingResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface CoinService {
    @GET("search/trending")
    suspend fun getTrends(): TrendingResponse

    @GET("coins/{id}")
    suspend fun getCoin(@Path("id") coinId: String): MainCoinResponse

    @GET("coins/list")
    suspend fun getList(): List<Item>
}
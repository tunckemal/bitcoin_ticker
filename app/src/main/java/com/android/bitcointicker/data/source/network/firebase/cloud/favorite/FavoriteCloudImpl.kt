package com.android.bitcointicker.data.source.network.firebase.cloud.favorite

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.data.source.network.firebase.auth.FirebaseAccount
import com.android.bitcointicker.data.source.network.firebase.cloud.base.FirebaseCloudImpl
import com.android.bitcointicker.domain.mapper.ID
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.QuerySnapshot

class FavoriteCloudImpl(private val firebaseAccount: FirebaseAccount) :
    FirebaseCloudImpl(firebaseAccount), FavoriteCloud {

    override fun get(data: (data: DataState<QuerySnapshot?>) -> Unit) {
        get(getRef(), data)
    }

    override fun saveFavorite(fieldMap: FieldMap, data: (data: DataState<Boolean?>) -> Unit) {
        delete(getRef(), fieldMap[ID].toString()) {
            save(getRef(), fieldMap, data)
        }
    }

    override fun favoriteCheck(id: String, check: (c: Boolean) -> Unit) {

        getRef().get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val q = task.result?.filter { it[ID] == id }
                check(q?.isNotEmpty() ?: false)
            } else {
                check(true)
            }
        }.addOnFailureListener {
            check(false)
        }
    }

    override fun removeFavorite(id: String, ok: () -> Unit) {
        delete(getRef(), id, ok)
    }

    private fun getRef(): CollectionReference {
        return ref().document(FAVORITE_PATH).collection(getUserId() ?: "")
    }

    companion object {
        const val FAVORITE_PATH = "favorites"
    }
}
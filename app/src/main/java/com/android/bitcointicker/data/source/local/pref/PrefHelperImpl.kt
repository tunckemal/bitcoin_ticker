package com.android.bitcointicker.data.source.local.pref


import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class PrefHelperImpl @Inject constructor(context: Context) : PrefHelper {

    private var mPrefs: SharedPreferences =
        context.getSharedPreferences(PREFHELPER, Context.MODE_PRIVATE)


    override fun clear() {
        mPrefs.edit().clear().apply()
    }

    override fun getAllowNotification(): Boolean {
        return mPrefs.getBoolean(ALLOW_NOTIFICATION_PREF_KEY, true)
    }

    override fun setAllowNotification(bool: Boolean) {
        mPrefs.edit().putBoolean(ALLOW_NOTIFICATION_PREF_KEY, bool).apply()
    }

    companion object {
        const val PREFHELPER = "Pref"
        const val ALLOW_NOTIFICATION_PREF_KEY = "allow_notication_pref_key"
    }
}
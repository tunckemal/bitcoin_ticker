package com.android.bitcointicker.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.bitcointicker.data.source.local.dao.model.CoinModel

@Dao
interface BitCoinDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(coin: CoinModel)


    @Query("SELECT * FROM coin_table")
    suspend fun get(): List<CoinModel>

    @Query("SELECT * FROM coin_table WHERE LOWER(name) LIKE '%' || :query || '%' OR LOWER(symbol) LIKE '%' || :query || '%'")
    suspend fun search(query: String): List<CoinModel>
}
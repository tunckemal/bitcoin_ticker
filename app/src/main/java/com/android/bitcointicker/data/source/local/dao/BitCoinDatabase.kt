package com.android.bitcointicker.data.source.local.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.bitcointicker.data.source.local.dao.model.CoinModel

@Database(entities = [CoinModel::class], version = 1)
abstract class BitCoinDatabase : RoomDatabase() {

    abstract fun bitCoinDao(): BitCoinDao

    companion object {
        val DATABASE_NAME: String = "bitcoin_db"
    }

}
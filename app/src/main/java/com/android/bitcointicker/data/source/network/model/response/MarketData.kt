package com.android.bitcointicker.data.source.network.model.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MarketData(
    @Json(name = "current_price")
    var currentPrice: CurrentPrice?,
    @Json(name = "price_change_percentage_24h")
    var price_change_percentage_24h: Double?

)
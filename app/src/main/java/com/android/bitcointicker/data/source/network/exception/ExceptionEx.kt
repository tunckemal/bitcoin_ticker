package com.android.bitcointicker.data.source.network.exception

fun Exception?.ex(): ErrorModel {
    return ApiErrorHandle().traceErrorException(this)
}
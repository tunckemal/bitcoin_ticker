package com.android.bitcointicker.data.source.network.firebase.cloud.favorite

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.data.source.network.firebase.cloud.base.FirebaseCloud
import com.google.firebase.firestore.QuerySnapshot

interface FavoriteCloud : FirebaseCloud {
    fun favoriteCheck(id: String, check: (c: Boolean) -> Unit)

    fun get(data: (data: DataState<QuerySnapshot?>) -> Unit)

    fun saveFavorite(fieldMap: FieldMap, data: (data: DataState<Boolean?>) -> Unit)

    fun removeFavorite(id: String,  ok: () -> Unit)
}
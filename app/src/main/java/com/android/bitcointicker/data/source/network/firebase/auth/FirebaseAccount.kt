package com.android.bitcointicker.data.source.network.firebase.auth

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.google.firebase.auth.FirebaseUser

interface FirebaseAccount {

    fun getCurrentUser(): FirebaseUser?

    fun getUserId(): String?

    fun isLoggedIn(): Boolean

    fun createNewAccount(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit)

    fun signIn(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit)

    fun logout()
}
package com.android.bitcointicker.data.source.network.model.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PingResponse(
    @Json(name = "gecko_says")
    var geckoSays: String?
)
package com.android.bitcointicker.data.source.network.service

import com.android.bitcointicker.data.source.network.model.response.PingResponse
import retrofit2.http.GET

interface ServerService {
    @GET("ping")
    suspend fun checkServer(): PingResponse
}
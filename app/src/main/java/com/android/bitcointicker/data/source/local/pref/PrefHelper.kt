package com.android.bitcointicker.data.source.local.pref

interface PrefHelper {
    fun getAllowNotification():Boolean
    fun setAllowNotification(bool:Boolean)

    fun clear()
}
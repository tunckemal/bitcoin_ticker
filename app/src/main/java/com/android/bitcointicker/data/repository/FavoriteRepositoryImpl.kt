package com.android.bitcointicker.data.repository

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.data.source.network.firebase.cloud.favorite.FavoriteCloud
import com.android.bitcointicker.domain.repository.FavoriteRepository
import com.google.firebase.firestore.QuerySnapshot

class FavoriteRepositoryImpl(
    private val favoriteCloud: FavoriteCloud
) : FavoriteRepository {
    override fun getFavorite(data: (data: DataState<QuerySnapshot?>) -> Unit) {
        favoriteCloud.get(data)
    }

    override fun saveFavorite(fieldMap: FieldMap, data: (data: DataState<Boolean?>) -> Unit) {
        favoriteCloud.saveFavorite(fieldMap, data)
    }

    override fun favoriteCheck(id: String, check: (c: Boolean) -> Unit) {
        favoriteCloud.favoriteCheck(id, check)
    }

    override fun removeFavorite(id: String, ok: () -> Unit) {
        favoriteCloud.removeFavorite(id, ok)
    }

}
package com.android.bitcointicker.data.source.network.model.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TrendingResponse(
    @Json(name = "coins")
    var coins: List<Coin>?,
    @Json(name = "exchanges")
    var exchanges: List<Any>?
)
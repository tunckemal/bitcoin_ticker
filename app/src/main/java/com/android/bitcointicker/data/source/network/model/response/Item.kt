package com.android.bitcointicker.data.source.network.model.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Item(
    @Json(name = "id")
    var id: String?,
    @Json(name = "large")
    var large: String?,
    @Json(name = "market_cap_rank")
    var marketCapRank: Int?,
    @Json(name = "name")
    var name: String?,
    @Json(name = "score")
    var score: Int?,
    @Json(name = "symbol")
    var symbol: String?,
    @Json(name = "thumb")
    var thumb: String?
)
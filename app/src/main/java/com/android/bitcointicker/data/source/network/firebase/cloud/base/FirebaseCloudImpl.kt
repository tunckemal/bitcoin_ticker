package com.android.bitcointicker.data.source.network.firebase.cloud.base

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.data.source.network.exception.ex
import com.android.bitcointicker.data.source.network.firebase.auth.FirebaseAccount
import com.android.bitcointicker.domain.mapper.ID
import com.android.bitcointicker.utility.constants.AppConstants
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot

open class FirebaseCloudImpl(private val firebaseAccount: FirebaseAccount) : FirebaseCloud {

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()


    override fun ref(): CollectionReference {
        return db.collection(AppConstants.COLLECTION_NAME)
    }

    override fun getUserId(): String? {
        return firebaseAccount.getUserId()
    }

    override fun get(ref: CollectionReference, data: (data: DataState<QuerySnapshot?>) -> Unit) {
        data(DataState.Loading)

        ref.get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    data(DataState.Success(task.result))
                } else {
                    data(DataState.Error(task.exception.ex()))
                }
            }.addOnFailureListener {
                data(DataState.Error(it.ex()))
            }
    }

    override fun save(
        ref: CollectionReference,
        fieldMap: FieldMap,
        data: (data: DataState<Boolean?>) -> Unit
    ) {
        data(DataState.Loading)

        ref.add(fieldMap)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    data(DataState.Success(true))
                } else {
                    data(DataState.Error(task.exception.ex()))
                }
            }.addOnFailureListener {
                data(DataState.Error(it.ex()))
            }
    }


    override fun delete(ref: CollectionReference, id: String, ok: () -> Unit) {
        ref.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val q = task.result?.filter { it[ID] == id }
                q?.forEach {
                    it.reference.delete()
                }
                ok()
            } else {
                ok()
            }
        }.addOnFailureListener {
            ok()
        }
    }

    override fun deleteAll(ref: CollectionReference, ok: () -> Unit) {

    }
}
package com.android.bitcointicker.data.source.local.dao.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "coin_table")
data class CoinModel(
    @PrimaryKey
    @ColumnInfo(name = "id")
    var coinId: String,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "symbol")
    var symbol: String
)
package com.android.bitcointicker.data.source.network.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
data class Image(
   @Json(name="large")
    var large: String?,
   @Json(name="small")
    var small: String?,
   @Json(name="thumb")
    var thumb: String?
)
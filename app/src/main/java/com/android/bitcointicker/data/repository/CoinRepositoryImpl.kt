package com.android.bitcointicker.data.repository

import com.android.bitcointicker.data.source.local.dao.BitCoinDao
import com.android.bitcointicker.data.source.local.dao.model.CoinModel
import com.android.bitcointicker.data.source.network.model.response.MainCoinResponse
import com.android.bitcointicker.data.source.network.model.response.TrendingResponse
import com.android.bitcointicker.data.source.network.service.CoinService
import com.android.bitcointicker.domain.mapper.toCoinModel
import com.android.bitcointicker.domain.repository.CoinRepository

class CoinRepositoryImpl(private val coinService: CoinService, private val bitCoinDao: BitCoinDao) :
    CoinRepository {
    override suspend fun trendingCoins(): TrendingResponse {
        return coinService.getTrends()
    }

    override suspend fun getCoinDetail(coinId: String): MainCoinResponse {
        return coinService.getCoin(coinId)
    }

    override suspend fun getList(): Boolean {

        val localData = bitCoinDao.get()
        if (localData.isEmpty()) {
            val data = coinService.getList()
            data.forEach {
                bitCoinDao.insert(it.toCoinModel())
            }

        }
        return true
    }

    override suspend fun search(query: String): List<CoinModel> {
        return bitCoinDao.search(query.toLowerCase())
    }

    override suspend fun checkLocalDb(): Boolean {
        val localData = bitCoinDao.get()

        return localData.isNotEmpty()
    }
}


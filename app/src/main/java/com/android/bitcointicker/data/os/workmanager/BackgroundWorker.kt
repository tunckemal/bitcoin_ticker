package com.android.bitcointicker.data.os.workmanager

import android.content.Context
import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.*
import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.data.os.notification.Notification
import com.android.bitcointicker.domain.usecase.CoinUseCase
import com.android.bitcointicker.domain.usecase.FavoriteUseCase
import com.bumptech.glide.load.HttpException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit

class BackgroundWorker @WorkerInject constructor(
    private val coinUseCase: CoinUseCase,
    private val favoriteUseCase: FavoriteUseCase,
    private val notification: Notification,
    @Assisted private val ctx: Context,
    @Assisted params: WorkerParameters
) : CoroutineWorker(ctx, params) {


    override suspend fun doWork(): Result {
        return withContext(Dispatchers.IO) {
            try {
                Log.d(TAG, "run")

                favoriteUseCase.getFavorite { firebase ->
                    if (firebase is DataState.Success) {

                        firebase.data?.forEach { fb ->

                            GlobalScope.launch {
                                try {
                                    val api = coinUseCase.getCoinDetail(fb.id ?: "")
                                    Log.d(TAG, "callapi")
                                    if (fb.currentPrice ?: 0.0 != api.currentPrice ?: 0.0) {
                                        notification.showNotification(
                                            ctx,
                                            "Bir değişiklik var",
                                            "Favorilerinize eklediğiniz ${fb.name} isimli datada bir değişiklik var, hemen şimdi bakabilirsin"
                                        )
                                        return@launch
                                    }
                                } catch (e: HttpException) {
                                    Log.d(TAG, e.localizedMessage ?: "")
                                } catch (e: Throwable) {
                                    Log.d(TAG, e.localizedMessage ?: "")
                                }
                            }
                        }

                    }
                }

                Result.success()
            } catch (e: Exception) {
                Log.d(TAG, "fail")
                Log.d(TAG, e.message.toString())
                Result.failure()

            }
        }
    }


    companion object {
        private const val TAG = "BackgroundWorker"
        private const val DEFAULT_MIN_INTERVAL = 15L


        @JvmStatic
        fun schedule(context: Context) {
            val worker =
                PeriodicWorkRequestBuilder<BackgroundWorker>(DEFAULT_MIN_INTERVAL, TimeUnit.MINUTES)
                    .addTag(TAG).build()
            WorkManager.getInstance(context)
                .enqueueUniquePeriodicWork(TAG, ExistingPeriodicWorkPolicy.REPLACE, worker)
        }


        @JvmStatic
        fun cancel(context: Context) {
            WorkManager.getInstance(context).cancelAllWorkByTag(TAG)
        }

        @JvmStatic
        fun state(context: Context) {
            val statuses = WorkManager.getInstance(context).getWorkInfosByTag(TAG).get()

            try {
                for (state in statuses) {
                    Log.d(TAG, "STATE = ${state.state}")
                }

            } catch (e: ExecutionException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }


}
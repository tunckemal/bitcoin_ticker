package com.android.bitcointicker.data.source.network.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
data class MainCoinResponse(
   @Json(name="hashing_algorithm")
    var hashingAlgorithm: String?,
   @Json(name="id")
    var id: String?,
   @Json(name="name")
    var name: String?,
   @Json(name="symbol")
    var symbol: String?,
   @Json(name="description")
    var description: Description?,
   @Json(name="image")
    var image: Image?,
   @Json(name="market_data")
    var marketData: MarketData?

)
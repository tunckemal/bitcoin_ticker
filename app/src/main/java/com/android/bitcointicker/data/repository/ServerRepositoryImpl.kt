package com.android.bitcointicker.data.repository

import com.android.bitcointicker.data.source.network.model.response.PingResponse
import com.android.bitcointicker.data.source.network.service.ServerService
import com.android.bitcointicker.domain.repository.ServerRepository

class ServerRepositoryImpl(private val serverService: ServerService) : ServerRepository {

    override suspend fun checkServer(): PingResponse {
        return serverService.checkServer()
    }

}


package com.android.bitcointicker.di

import android.content.Context
import androidx.room.Room
import com.android.bitcointicker.data.source.local.dao.BitCoinDao
import com.android.bitcointicker.data.source.local.dao.BitCoinDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class RoomModule {
    @Singleton
    @Provides
    fun provideCoinDb(@ApplicationContext context: Context): BitCoinDatabase {
        return Room
            .databaseBuilder(
                context,
                BitCoinDatabase::class.java,
                BitCoinDatabase.DATABASE_NAME
            )
            .fallbackToDestructiveMigration()
            .build()
    }


    @Singleton
    @Provides
    fun provideCoinDAO(database: BitCoinDatabase): BitCoinDao {
        return database.bitCoinDao()
    }
}
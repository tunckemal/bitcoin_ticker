package com.android.bitcointicker.di

import com.android.bitcointicker.data.source.network.service.CoinService
import com.android.bitcointicker.data.source.network.service.ServerService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ApiModule {

    @Singleton
    @Provides
    fun provideCoinService(retrofit: Retrofit): CoinService =
        retrofit.create(CoinService::class.java)

    @Singleton
    @Provides
    fun provideServerService(retrofit: Retrofit): ServerService =
        retrofit.create(ServerService::class.java)

}
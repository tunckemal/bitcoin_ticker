package com.android.bitcointicker.di

import android.content.Context
import com.android.bitcointicker.data.os.notification.Notification
import com.android.bitcointicker.data.source.local.pref.PrefHelper
import com.android.bitcointicker.data.source.local.pref.PrefHelperImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideSharedPref(@ApplicationContext context: Context): PrefHelper =
        PrefHelperImpl(context)


    @Provides
    @Singleton
    fun provideNotification(prefHelper: PrefHelper): Notification =
        Notification(prefHelper)
}
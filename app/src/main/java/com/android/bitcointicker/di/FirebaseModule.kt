package com.android.bitcointicker.di

import com.android.bitcointicker.data.source.network.firebase.auth.FirebaseAccount
import com.android.bitcointicker.data.source.network.firebase.auth.FirebaseAccountImpl
import com.android.bitcointicker.data.source.network.firebase.cloud.favorite.FavoriteCloud
import com.android.bitcointicker.data.source.network.firebase.cloud.favorite.FavoriteCloudImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class FirebaseModule {

    @Singleton
    @Provides
    fun provideFirebaseAccount(): FirebaseAccount {
        return FirebaseAccountImpl()
    }


    @Singleton
    @Provides
    fun provideFavoriteCloud(firebaseAccount: FirebaseAccount): FavoriteCloud {
        return FavoriteCloudImpl(firebaseAccount)
    }
}
package com.android.bitcointicker.di


import com.android.bitcointicker.data.repository.AccountRepositoryImpl
import com.android.bitcointicker.data.repository.CoinRepositoryImpl
import com.android.bitcointicker.data.repository.FavoriteRepositoryImpl
import com.android.bitcointicker.data.repository.ServerRepositoryImpl
import com.android.bitcointicker.data.source.local.dao.BitCoinDao
import com.android.bitcointicker.data.source.network.firebase.auth.FirebaseAccount
import com.android.bitcointicker.data.source.network.firebase.cloud.favorite.FavoriteCloud
import com.android.bitcointicker.data.source.network.service.CoinService
import com.android.bitcointicker.data.source.network.service.ServerService
import com.android.bitcointicker.domain.repository.AccountRepository
import com.android.bitcointicker.domain.repository.CoinRepository
import com.android.bitcointicker.domain.repository.FavoriteRepository
import com.android.bitcointicker.domain.repository.ServerRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideCoinRepository(
        coinService: CoinService,
        bitCoinDao: BitCoinDao
    ): CoinRepository {
        return CoinRepositoryImpl(coinService, bitCoinDao)
    }

    @Singleton
    @Provides
    fun provideServerRepository(
        serverService: ServerService
    ): ServerRepository {
        return ServerRepositoryImpl(serverService)
    }


    @Singleton
    @Provides
    fun provideUserRepository(
        firebaseAccount: FirebaseAccount
    ): AccountRepository {
        return AccountRepositoryImpl(firebaseAccount)
    }

    @Singleton
    @Provides
    fun provideFavoriteRepository(
        favoriteCloud: FavoriteCloud
    ): FavoriteRepository {
        return FavoriteRepositoryImpl(favoriteCloud)
    }
}
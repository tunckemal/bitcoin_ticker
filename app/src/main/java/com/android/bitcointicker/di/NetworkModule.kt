package com.android.bitcointicker.di

import android.app.Application
import android.content.Context
import com.android.bitcointicker.BuildConfig
import com.android.bitcointicker.data.source.local.pref.PrefHelper
import com.android.bitcointicker.utility.extension.isNetworkAvailable
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Cache
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(
        application: Application,
        prefHelper: PrefHelper,
        @ApplicationContext context: Context
    ): OkHttpClient {
        val client = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS)
            .cache(Cache(context.cacheDir, CACHE_SIZE))
            .addInterceptor(retrofitLogging())

        client.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .headers(getJsonHeader(context,prefHelper))
                .build()
            chain.proceed(request)
        }.build()

        return client.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .build()
    }


    private fun getJsonHeader(context: Context, prefHelper: PrefHelper): Headers {
        val builder = Headers.Builder().apply {
            add("Content-Type", "application/json")
            add("Accept", "application/json")

            if (!context.isNetworkAvailable()) {
                add(
                    "Cache-Control",
                    "public, only-if-cached, max-stale=$MAX_STALE"
                )
            }
        }



        return builder.build()
    }

    @Provides
    fun createMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        .build()

    @Provides
    fun createMoshiConverterFactory(): MoshiConverterFactory = MoshiConverterFactory.create()


    private fun retrofitLogging(): Interceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return loggingInterceptor
    }


    companion object {
        const val CACHE_SIZE: Long = 10 * 1024 * 1024
        const val MAX_STALE: Int = 60 * 60 * 24 * 28

        const val CONNECT_TIME_OUT: Long = 30L
        const val READ_TIME_OUT: Long = 30L
        const val WRITE_TIME_OUT: Long = 30L
    }

}


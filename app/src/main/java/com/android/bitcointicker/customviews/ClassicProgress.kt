package com.android.bitcointicker.customviews

import android.content.Context
import android.util.AttributeSet
import androidx.core.widget.ContentLoadingProgressBar

class ClassicProgress : ContentLoadingProgressBar {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init()
    }

    private fun init() {

    }
}
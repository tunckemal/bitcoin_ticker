package com.android.bitcointicker.customviews


import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.android.bitcointicker.R
import com.google.android.material.button.MaterialButton

class ClassicMaterialButton : MaterialButton {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init()
    }

    private fun init() {
        this.apply {
            setBackgroundColor(ContextCompat.getColor(context, R.color.red))
            setTextColor(ContextCompat.getColor(context, R.color.white))
        }
    }
}
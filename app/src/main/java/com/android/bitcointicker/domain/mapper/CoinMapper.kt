package com.android.bitcointicker.domain.mapper

import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.data.source.local.dao.model.CoinModel
import com.android.bitcointicker.data.source.network.model.response.Item
import com.android.bitcointicker.data.source.network.model.response.MainCoinResponse
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.presentation.adapter.CoinAdapter
import com.google.firebase.firestore.DocumentSnapshot


const val ID = "id"
const val NAME = "name"
const val SYMBOL = "symbol"
const val THUMB = "thumb"
const val RANK = "rank"
const val PRICE = "price"

fun Item.toCoinEntity() = CoinEntity(
    id = this.id,
    name = this.name,
    symbol = this.symbol,
    thumb = this.thumb,
    rank = this.marketCapRank ?: 0
)


fun CoinEntity.toFieldMap(): FieldMap {
    val fieldMap = FieldMap()

    this.apply {
        fieldMap[ID] = id ?: ""
        fieldMap[NAME] = name ?: ""
        fieldMap[SYMBOL] = symbol ?: ""
        fieldMap[THUMB] = thumb ?: ""
        fieldMap[PRICE] = currentPrice ?: 0.0
    }

    return fieldMap
}

fun DocumentSnapshot.toCoinEntity() = CoinEntity(
    id = this[ID].toString(),
    name = this[NAME].toString(),
    symbol = this[SYMBOL].toString(),
    thumb = this[THUMB].toString(),
    rank = this.getLong(RANK)?.toInt(),
    currentPrice = this.getDouble(PRICE)
)

fun MainCoinResponse.toCoinEntity() = CoinEntity(
    id, name, symbol,
    description = this.description?.tr,
    img = this.image?.large,
    thumb = this.image?.thumb,
    hashingal = this.hashingAlgorithm,
    currentPrice = this.marketData?.currentPrice?.tryX ?: 0.0,
    priceChange24h = this.marketData?.price_change_percentage_24h ?: 0.0
)


fun CoinModel.toCoinEntity() = CoinEntity(
    id = this.coinId,
    name = this.name,
    symbol = this.symbol
).also { coin ->
    coin.type = CoinAdapter.SearchViewHolder.TYPE
}

fun Item.toCoinModel() = CoinModel(
    coinId = this.id ?: "",
    name = this.name ?: "",
    symbol = this.symbol ?: ""
)
package com.android.bitcointicker.domain.entity

import com.android.bitcointicker.core.adapter.RecyclerItem
import com.android.bitcointicker.presentation.adapter.CoinAdapter

data class CoinEntity(
    val id: String? = "",
    val name: String? = "",
    var symbol: String? = "",
    var thumb: String? = "",
    var rank: Int? = 0,
    var img: String? = "",
    var description: String? = "",
    var hashingal: String? = "",
    var currentPrice: Double? = 0.0,
    var priceChange24h: Double? = 0.0
) : RecyclerItem(CoinAdapter.CoinViewHolder.TYPE, id ?: "")
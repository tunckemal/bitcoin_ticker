package com.android.bitcointicker.domain.usecase

import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.domain.mapper.toCoinEntity
import com.android.bitcointicker.domain.repository.CoinRepository
import javax.inject.Inject

class CoinUseCase @Inject constructor(private val coinRepository: CoinRepository) {

    suspend fun getTrends(): List<CoinEntity> {

        val data = coinRepository.trendingCoins()
        return data.coins?.map {
            it.item?.toCoinEntity()
        } as List<CoinEntity>? ?: emptyList()
    }

    suspend fun getCoinDetail(id: String): CoinEntity {
        return coinRepository.getCoinDetail(id).toCoinEntity()
    }

    suspend fun getList(): Boolean {
        return coinRepository.getList()
    }

    suspend fun search(query: String): List<CoinEntity> {
        return coinRepository.search(query).map {
            it.toCoinEntity()
        }
    }

    suspend fun checkLocalDb(): Boolean {
        return coinRepository.checkLocalDb()
    }
}

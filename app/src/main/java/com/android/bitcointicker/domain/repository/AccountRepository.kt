package com.android.bitcointicker.domain.repository

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.google.firebase.auth.FirebaseUser

interface AccountRepository {
    fun getCurrentUser(): FirebaseUser?

    fun isLoggedIn(): Boolean

    fun logout()

    fun createNewAccount(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit)

    fun signIn(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit)
}
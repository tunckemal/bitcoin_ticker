package com.android.bitcointicker.domain.entity

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

data class Favorite(
    var _isFavorite: Boolean? = false
) : BaseObservable() {

    var isFavorite: Boolean
        @Bindable get() = _isFavorite ?: false
        set(value) {
            _isFavorite = value
            notifyPropertyChanged(BR.favorite)
        }
}
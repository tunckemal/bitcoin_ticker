package com.android.bitcointicker.domain.repository

import com.android.bitcointicker.data.source.local.dao.model.CoinModel
import com.android.bitcointicker.data.source.network.model.response.MainCoinResponse
import com.android.bitcointicker.data.source.network.model.response.TrendingResponse

interface CoinRepository {
    suspend fun trendingCoins(): TrendingResponse

    suspend fun getCoinDetail(coinId: String): MainCoinResponse

    suspend fun getList(): Boolean

    suspend fun search(query: String): List<CoinModel>

    suspend fun checkLocalDb():Boolean
}
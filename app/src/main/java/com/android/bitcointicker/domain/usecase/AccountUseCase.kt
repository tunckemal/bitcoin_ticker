package com.android.bitcointicker.domain.usecase

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.domain.repository.AccountRepository
import com.google.firebase.auth.FirebaseUser
import javax.inject.Inject

class AccountUseCase @Inject constructor(private val userRepository: AccountRepository) {


    fun isLoggedIn() = userRepository.isLoggedIn()

    fun getCurrentUser() = userRepository.getCurrentUser()

    fun createNewAccount(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit) =
        userRepository.createNewAccount(fieldMap, data)

    fun signIn(fieldMap: FieldMap, data: (data: DataState<FirebaseUser?>) -> Unit) =
        userRepository.signIn(fieldMap, data)

    fun logout(){
        userRepository.logout()
    }
}
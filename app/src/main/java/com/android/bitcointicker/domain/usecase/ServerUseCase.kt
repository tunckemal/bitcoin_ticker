package com.android.bitcointicker.domain.usecase

import com.android.bitcointicker.domain.repository.ServerRepository
import javax.inject.Inject

class ServerUseCase @Inject constructor(private val serverRepository: ServerRepository) {

    suspend fun checkServer(): String {
        return serverRepository.checkServer().geckoSays ?: ""
    }

}
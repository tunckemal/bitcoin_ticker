package com.android.bitcointicker.domain.usecase

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.android.bitcointicker.core.handle
import com.android.bitcointicker.domain.entity.CoinEntity
import com.android.bitcointicker.domain.mapper.toCoinEntity
import com.android.bitcointicker.domain.repository.FavoriteRepository
import javax.inject.Inject

class FavoriteUseCase @Inject constructor(private val favoriteRepository: FavoriteRepository) {

    fun getFavorite(data: (data: DataState<List<CoinEntity>?>) -> Unit) {
        favoriteRepository.getFavorite {
            it.handle({
                data(DataState.Loading)
            }, { query ->

                val coinEntity = query?.map { q ->
                    q.toCoinEntity()
                } ?: emptyList()

                data(DataState.Success(coinEntity))

            }, { error ->
                data(DataState.Error(error))
            })
        }
    }

    fun saveFavorite(fieldMap: FieldMap, data: (data: DataState<Boolean?>) -> Unit) {
        favoriteRepository.saveFavorite(fieldMap, data)
    }

    fun favoriteCheck(id: String, check: (c: Boolean) -> Unit) {
        favoriteRepository.favoriteCheck(id, check)
    }

    fun removeFavorite(id: String, ok: () -> Unit) {
        favoriteRepository.removeFavorite(id, ok)
    }
}
package com.android.bitcointicker.domain.repository

import com.android.bitcointicker.core.DataState
import com.android.bitcointicker.core.FieldMap
import com.google.firebase.firestore.QuerySnapshot

interface FavoriteRepository {
    fun getFavorite(data: (data: DataState<QuerySnapshot?>) -> Unit)
    fun saveFavorite(fieldMap: FieldMap, data: (data: DataState<Boolean?>) -> Unit)

    fun removeFavorite(id: String,  ok: () -> Unit)

    fun favoriteCheck(id: String, check: (c: Boolean) -> Unit)
}
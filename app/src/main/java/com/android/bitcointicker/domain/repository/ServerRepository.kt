package com.android.bitcointicker.domain.repository

import com.android.bitcointicker.data.source.network.model.response.PingResponse

interface ServerRepository {
    suspend fun checkServer(): PingResponse
}
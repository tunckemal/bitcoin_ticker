PROJE ÖZETİ

Proje Kotlin dili ile yazılmıştır. Design pattern olarak MVVM kullanıldı. Projede katmanları daha rahat yönetmek için Clean Architecture yaklaşımı kullanılmıştır.Uygulamada Firebase Authentication vardır.
Api'den çekilen Trend Bitcoinleri favoriye eklenip çıkarılabilmesi için Firebase Firestore kullanışmıştır. Api' de arama servisi olmadığı için kullanıcı arama ekranını ilk açtığında Api' deki 
listelemeden gelen bütün veriler çekilip Local veritabanına kaydedilmiştir.Local veritabanındaki veriler silinene kadar listeleme Apisine istek atılmayacaktır. Burada Room kullanılmıştır.

Projede WorkManager vardır. Her on beş dakikada bir uygulama kapalı olsa dahi kullanıcın favorilerine eklenen Bitcoinlerin fiyatlarında değişiklik olup olmadığı Api'den kontrol edilmiştir. Bir değişik var ise
kullanıcıya bildirim ile bildirilmiştir. Kullanıcı dilerse profilden bildirimi kapatabilir.

Başlıca kullanılan kütüphaneler;MVMM, Databinding, Dagger HILT, Flow, Retrofit, Moshi, Room, WorkManager, Glide
